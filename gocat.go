package main

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"sync/atomic"
	"time"
)

var UID = os.Getuid()
var activeConns int64

func pump(a, b net.Conn) {
	defer a.Close()
	defer b.Close()

	buf := make([]byte, 8192)
	for {
		n, err := a.Read(buf)
		if err != nil {
			log.Println("error reading from socket", err)
			break
		}

		_, err = b.Write(buf[:n])
		if err != nil {
			log.Println("error writing to socket", err)
			break
		}
	}
}

func authoriseSocket(l net.Conn) error {
	local, err := AddrToHex(l.LocalAddr())
	if err != nil {
		return err
	}

	remote, err := AddrToHex(l.RemoteAddr())
	if err != nil {
		return err
	}

	t, err := NewSocketTable()
	if err != nil {
		return err
	}
	defer t.Close()

	for t.Next() {
		v, err := t.Value()
		if err != nil {
			log.Println("error parsing entry", v)
			continue
		}
		if v.Local == remote && v.Remote == local { // other process with source uid has local/remote swapped
			if v.Uid == UID {
				return nil
			}
			return fmt.Errorf("bad uid %d", v.Uid)
		}
	}

	return errors.New("couldn't find address in connection table")
}

func handleConnection(l net.Conn, target string) {
	defer atomic.AddInt64(&activeConns, -1);
	defer l.Close()
	err := authoriseSocket(l)
	if err != nil {
		log.Println("unauthorised connection, closing", err)
		return
	}
	log.Println("accepted connection from", l.RemoteAddr())

	nc, err := net.Dial("unix", target)
	if err != nil {
		log.Println("unable to connect", err)
		return
	}
	defer nc.Close()

	go pump(nc, l)
	pump(l, nc)
}

func reaper() {
	inactive := false
	for {
		time.Sleep(60 * time.Second)
		if atomic.LoadInt64(&activeConns) == 0 {
			if inactive {
				log.Println("idle -- exiting")
				os.Exit(0)
			}
			inactive = true
		} else {
			inactive = false
		}
	}
}

func main() {
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "usage: %s [listen addr (tcp)|systemd] [target addr (unix)]\n", os.Args[0])
		os.Exit(1)
	}

	listen := os.Args[1]
	target := os.Args[2]

	var l net.Listener
	if listen == "systemd" {
		listeners, err := Listeners()
		if err != nil {
			log.Fatal(err)
		}
		if len(listeners) != 1 {
			log.Fatalf("unexpected number of socket activation (%d != 1)", len(listeners))
		}
		l = listeners[0]

		go reaper()
	} else {
		tcp_l, err := net.Listen("tcp", listen)
		if err != nil {
			log.Fatal(err)
		}
		l = tcp_l
	}

	for {
		c, err := l.Accept()
		if err != nil {
			log.Println("unable to accept connection", err)
			continue
		}

		atomic.AddInt64(&activeConns, 1);
		go handleConnection(c, target)
	}
}
