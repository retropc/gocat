all: gocat

.PHONY: clean

gocat: gocat.go sockettable.go systemd.go
	go build -o $@ $^

clean:
	rm -f gocat
